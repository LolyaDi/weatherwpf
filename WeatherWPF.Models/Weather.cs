﻿using Newtonsoft.Json;

namespace WeatherWPF.Models
{
    public class Weather
    {
        [JsonProperty("forecast")]
        public Forecast Forecast { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace WeatherWPF.Models
{
    public class Day
    {
        [JsonProperty("maxtemp_c")]
        public double? MaximumTemperature { get; set; }

        [JsonProperty("mintemp_c")]
        public double? MinimumTemperature { get; set; }

        [JsonProperty("maxwind_kph")]
        public double? WindSpeed { get; set; }

        [JsonProperty("avghumidity")]
        public double? AverageHumidity { get; set; }

        [JsonProperty("totalprecip_mm")]
        public double? TotalPrecipitation { get; set; }

        [JsonProperty("avgvis_km")]
        public double? AverageVisible { get; set; }

        [JsonProperty("condition")]
        public Condition Condition { get; set; }
    }
}
